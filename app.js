var express = require('express')
var app = express()

var path = require('path')

var mongodb = require('mongodb')

var beacon = require('./routes/beacon.js')
var regions = require('./routes/regions.js')
var index = require('./routes/index.js')

//Custom class that performs connection to MongoDB
var connection = require('./connection.js')
var db = connection.connect()

app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use('/', index(db))
app.use('/beacons', beacon(db))
app.use('/regions', regions(db))
app.use('/public', express.static(__dirname + "/public"));

app.use(function(req, res, next) {
  res.json('404 - An error has ocurred!')
});

var server = app.listen(3000, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port)

})