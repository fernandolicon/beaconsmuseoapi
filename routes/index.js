var express = require('express')
var router = express.Router()

var _db = ""
var collection = ""

router.get('/', function(req, res, next) {
	regionCollection.find().toArray(function(err, regions){
		res.render('index', { title: 'Portal de Administración', regions : regions })
	})
})

router.get('/newregion', function(req, res, next) {
		res.render('newregion')
})

router.get('/editRegion/:regionID', function(req, res, next) {
	var regionID = new require('mongodb').ObjectID(req.params.regionID)
	
	regionCollection.findOne({"_id" : regionID}, function(err, region){
		var regionID = "" + region._id
		beaconInfoCollection.find({"regionID" : regionID}).toArray(function(err, beacons){
			res.render('editregion', { region: region, beacons: beacons})
		})
	})
})

router.get('/editRegion/:regionID/addNewBeacon', function(req, res, next) {
	var regionID = new require('mongodb').ObjectID(req.params.regionID)

	regionCollection.findOne({"_id" : regionID}, function(err, region){
		res.render('newbeacon', {region: region})
	})

})

router.get('/editRegion/:regionID/editBeacon/:beaconID', function(req, res, next) {
	var regionID = new require('mongodb').ObjectID(req.params.regionID)
	var beaconID = new require('mongodb').ObjectID(req.params.beaconID)

	regionCollection.findOne({"_id" : regionID}, function(err, region){
		beaconInfoCollection.findOne({"_id" : beaconID}, function(err, beacon){
			var fetchedBeaconID = "" + beacon._id
			beaconDetailCollection.findOne({"beaconID" : fetchedBeaconID}, function(err, beaconDetail){
				res.render('editbeacon', {region: region, beaconInfo: beacon, beaconDetail: beaconDetail})
			})
		})
	})
})

module.exports = function(db){
	_db = db
	regionCollection = _db.collection("regions")
	beaconInfoCollection = _db.collection("beaconInfo")
	beaconDetailCollection = _db.collection("beaconDetail")
	return router
}