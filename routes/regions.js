var express = require('express')
var router = express.Router()
var mkdirp = require('mkdirp')

var rimraf = require('rimraf')

var fs = require('fs')

var multer = require('multer')
router.use(multer({ dest: './temp/', 
	rename: function (fieldname, filename) {
		return fieldname
	},
	includeEmptyFields: true
}))

var mongodb = require('mongodb')

var _db = ""
var collection = ""

router.get('/all', function(req, res, next) {
	collection.find().toArray(function(err, items){
		res.json(items)
	})
})

router.get('/whereUUID/:uuid', function(req, res, next) {
	var uuid = req.params.uuid
	collection.findOne({"UUID" : uuid}, function(err, region){
		res.json(region)
	})
})

router.post('/addRegion', function(req, res) {
	var newRegion = req.body

	//First we create the folders for region and beacons
	var folderPath = './public/images/' + newRegion.name + '/'
	mkdirp(folderPath)
	
	var beaconPath = folderPath + "beaconsImages"
	mkdirp(beaconPath)

	//We check if image was uploaded
	if (!isEmpty(req.files)){
		var image = req.files.Image
		var oldPath = './temp/'
		//It's better to have a generic name instead of a godknowswhat name
		var newImageName =  "regionImage." + image.extension
		
		//We move the image from temporal folder to region folder
		fs.renameSync(oldPath + image.name, folderPath + newImageName)

		newRegion.image = newImageName
	}

	//Create document
	collection.insertOne(newRegion, function(err, result){
		if (err == null){
			res.render('saved', { text: 'La información ha sido guardada.'})
		}else{
			res.render('saved', { text: 'Hubo un error al guardar los datos. ' + err})
		}
	})
})

router.post('/editRegion/:id', function(req, res) {
	var objectId = new require('mongodb').ObjectID(req.params.id)

	var regionChanges = req.body

	//We need to have the original region before changes
	collection.findOne({"_id" : objectId}, function(err, region){
		//Check if any change was made, in case it wasn't tell the user
		if (checkChanges(region, regionChanges, req.files)){
			var path = './public/images/' + region.name + '/'
			//We check if the name has changed so we change the region folder in case it was
			if (regionChanges.name != region.name){
				var newNamePath = './public/images/' + regionChanges.name + '/'
				fs.rename(path, newNamePath)
				path = newNamePath
			}

			//Check if image was changed
			if (!isEmpty(req.files)){
				//We delete current image if it exists, just if format has changed i.e. .jpg to .png
				var actualImagePath = path + region.image
				if(fs.existsSync(actualImagePath)){
		    		fs.unlinkSync(actualImagePath, function(err) {
		    		})
		    	}

		    	var image = req.files.Image
				var oldPath = './temp/'
		    	var newImageName =  "regionImage." + image.extension
				fs.renameSync(oldPath + image.name, path + newImageName)

				regionChanges.image = newImageName
			}else{
				//In case it wasn't we put the image name in the new document
				regionChanges.image = region.image
			}

			collection.updateOne({"_id" : objectId}, regionChanges, function(err, result){
				if (err == null){
					res.render('saved', { text: 'La información ha sido guardada.'})
				}else{
					res.render('saved', { text: 'Hubo un error al guardar los datos. ' + err})
				}
			})
		}else{
			res.render('saved', { text: 'No se detectaron cambios en la región.'})
		}
	})
})

router.get('/deleteRegion/:regionID', function(req, res){
	var regionID = new require('mongodb').ObjectID(req.params.regionID),
		regionIDString = req.params.regionID
	var beaconInfoCollection = _db.collection("beaconInfo"),
		beaconDetailCollection = _db.collection("beaconDetail")

	collection.findOne({"_id" : regionID}, function(err, region){
		var path = './public/images/' + region.name
		rimraf(path, function(error){})
		beaconInfoCollection.find({"regionID" : regionIDString}).toArray(function(err, beacons){
			for (var i in beacons){
				beaconID = "" + beacons[i]._id
				beaconDetailCollection.deleteOne({"beaconID" : beaconID}, function(err, detailDeleted){})
			}
			beaconInfoCollection.deleteMany({"regionID" : regionIDString}, function(err, result){})
		})
	})

	collection.deleteOne({"_id" : regionID}, function(err, region){
		if (err == null){
			res.render('saved', { text: 'Se ha borrado la región.'})
		}else{
			res.render('saved', { text: 'Hubo un error con la eliminación ' + err})
		}
	})
})

function checkChanges(region, regionChanges, files){
	if (!isEmpty(files)){
		return true
	}
	for (var key in regionChanges) {
  		var regionValue = "" + region[key]
  		var changeValue  = "" + regionChanges[key]
  		if (regionValue != changeValue){
  			return true
  		}
	}
	return false
}

//Check if object is empty
var hasOwnProperty = Object.prototype.hasOwnProperty;
function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

module.exports = function(db){
	_db = db
	collection = _db.collection("regions")
	return router
}