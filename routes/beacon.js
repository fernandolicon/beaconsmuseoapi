var express = require('express')
var router = express.Router()

var mkdirp = require('mkdirp')

var fs = require('fs')

var multer = require('multer')
router.use(multer({ dest: './temp/', 
	rename: function (fieldname, filename) {
		return fieldname
	},
	includeEmptyFields: false
}))

var mongodb = require('mongodb')

var _db = ""
var collection = ""

router.get('/fromRegion/:regionID', function(req, res, next) {
	collection = _db.collection("beaconInfo")
	var regionID = req.params.regionID
	collection.find({"regionID" : regionID}).toArray(function(err, items){
		res.json(items)
	})
})

router.get('/detail/:beaconId', function(req, res, next) {
	collection = _db.collection("beaconDetail")
	var beaconId = req.params.beaconId
	collection.findOne({"beaconID" : beaconId}, function(err, beacon){
		res.json(beacon)
	})
})

router.post('/addBeacon/:regionName/:regionID', function(req, res) {
	var beacon = req.body

	//Populate objects
	regionID = req.params.regionID
	var beaconInfo = populateInfo(beacon, regionID),
		beaconDetail = populateDetail(beacon)

	//Get collections
	var infoCollection = _db.collection("beaconInfo"),
		detailCollection = _db.collection("beaconDetail")

	//Save images in other folder, temp folder must be empty
	if (!isEmpty(req.files)){
		var image = req.files.image,
			icon = req.files.icon

		var newIconName = moveImage(req.params.regionName, icon, beaconInfo.name, beaconInfo.minor),
			newImageName = moveImage(req.params.regionName, image, beaconInfo.name, beaconInfo.minor)

			beaconInfo.icon = newIconName
			beaconDetail.image = newImageName
	}	
	
	//Save documents in collections
	infoCollection.insertOne(beaconInfo, function(err, result){		
		var data = result.ops[0]
		beaconDetail.beaconID = "" + data._id
		detailCollection.insertOne(beaconDetail, function(err, result){
			if (err == null){
   				res.render('savedBeacon', { text: 'La información ha sido guardada.', regionID : regionID})
	   		}else{
	       		res.render('savedBeacon', { text: 'Hubo un error al guardar los datos. ' + err, regionID : regionID})
	       	}
		})
	})
})

router.post('/editBeacon/:regionName/:regionID/:beaconID', function(req, res){
	var beacon = req.body
	var beaconID = new require('mongodb').ObjectID(req.params.beaconID)

	//Populate objects
	regionID = req.params.regionID
	var newBeaconInfo = populateInfo(beacon, regionID),
		newBeaconDetail = populateDetail(beacon)

	//Get collections
	var infoCollection = _db.collection("beaconInfo"),
		detailCollection = _db.collection("beaconDetail")

	infoCollection.findOne({"_id" : beaconID}, function(err, oldBeaconInfo){
		var fetchedBeaconID = "" + oldBeaconInfo._id
		detailCollection.findOne({"beaconID" : fetchedBeaconID}, function(err, oldBeaconDetail){
			var beaconDetailID = new require('mongodb').ObjectID(oldBeaconDetail._id)

			var imagesChanged = !isEmpty(req.files),
				infoChanged = checkChanges(oldBeaconInfo, newBeaconInfo),
				detailChanged = checkChanges(oldBeaconDetail, newBeaconDetail)
			if (imagesChanged || infoChanged || detailChanged){
				//If image has changed we need to move it from temporal folder and delete the older image
					if (imagesChanged){
						var image = req.files.image,
							icon = req.files.icon

						if (isEmpty(image)){
							removeImage(req.params.regionName, oldBeaconInfo.icon)
							var newIconName = moveImage(req.params.regionName, icon, newBeaconInfo.name, newBeaconInfo.minor)
							newBeaconInfo.icon = newIconName
							newBeaconDetail.image = oldBeaconDetail.image
						}else if (isEmpty(icon)){
							removeImage(req.params.regionName, oldBeaconDetail.image)
							var newImageName = moveImage(req.params.regionName, image, newBeaconInfo.name, newBeaconInfo.minor)
							newBeaconInfo.icon = oldBeaconInfo.icon
							newBeaconDetail.image = newImageName
						}else{
							removeImage(req.params.regionName, oldBeaconInfo.icon)
							removeImage(req.params.regionName, oldBeaconDetail.image)
							var newIconName = moveImage(req.params.regionName, icon, newBeaconInfo.name, newBeaconInfo.minor),
								newImageName = moveImage(req.params.regionName, image, newBeaconInfo.name, newBeaconInfo.minor)
							newBeaconInfo.icon = newIconName
							newBeaconDetail.image = newImageName
						}
					}	
					else{
						newBeaconDetail.image = oldBeaconDetail.image
						newBeaconInfo.icon = oldBeaconInfo.icon
					}
					newBeaconDetail.beaconID = oldBeaconDetail.beaconID

					//Save documents in collections
					infoCollection.updateOne({"_id" : beaconID}, newBeaconInfo, function(err, result){
						detailCollection.updateOne({"_id" : beaconDetailID}, newBeaconDetail, function(err, result){
							if (err == null){
				   				res.render('savedBeacon', { text: 'La información ha sido guardada.', regionID : regionID})
					   		}else{
					       		res.render('savedBeacon', { text: 'Hubo un error al guardar los datos. ' + err, regionID : regionID})
					       	}
						})
					})	
			}else{
				res.render('savedBeacon', { text: 'No se detectaron cambios.', regionID : regionID})
			}
		})
	})

})

router.get('/deleteBeacon/:regionID/:regionName/:beaconID', function(req, res, next){
	var regionID = req.params.regionID,
		regionName = req.params.regionName,
		beaconID = new require('mongodb').ObjectID(req.params.beaconID),
		beaconIDString = req.params.beaconID
	//Get collections
	var infoCollection = _db.collection("beaconInfo"),
		detailCollection = _db.collection("beaconDetail")

	infoCollection.findOne({"_id" : beaconID}, function(err, beacon){
		var fetchedBeaconID = "" + beacon._id
		detailCollection.findOne({"beaconID" : fetchedBeaconID}, function(err, beaconDetail){
			removeImage(regionName, beacon.icon)
			removeImage(regionName, beaconDetail.image)
		})
	})

	infoCollection.deleteOne({"_id" : beaconID}, function(err, result, next){
		detailCollection.deleteOne({"beaconID" : beaconIDString}, function(err, detailDeleted){
			if (err == null){
				res.render('savedBeacon', { text: 'El beacon se ha borrado.', regionID : regionID})
			}else{
				res.render('savedBeacon', { text: 'Hubo un error con la eliminación ' + err, regionID : regionID})
			}
		})
	})
})

function moveImage(regionName, image, beaconName, beaconMinor){
	var oldPath = './temp/',
		newPath = './public/images/' + regionName + '/beaconsImages/'
	var newImageName = image.fieldname + beaconName + beaconMinor + '.' + image.extension
		
	newImageName = newImageName.replace(/ /g,'')

	fs.renameSync(oldPath + image.name, newPath + newImageName)

	return newImageName
}

function removeImage(regionName, imageName){
	var actualImagePath = './public/images/' + regionName + '/beaconsImages/' + imageName
	if(fs.existsSync(actualImagePath)){
		fs.unlinkSync(actualImagePath, function(err) {
		})
	}
}

function populateInfo(beacon, regionID){
	var beaconInfo = {}

	beaconInfo.major = beacon.major
	beaconInfo.minor = beacon.minor
	beaconInfo.name = beacon.name
	beaconInfo.regionID = regionID
	beaconInfo.alertTitle = beacon.alertTitle
	beaconInfo.alertMessage = beacon.alertMessage

	return beaconInfo
}

function populateDetail(beacon){
	var beaconDetail = {}

	var type = typeof(beacon.titleDescription)

	if (type == "string"){
		beaconDetail.descriptionTitles = []
		beaconDetail.descriptions = []
		beaconDetail.descriptionTitles[0] = beacon.titleDescription
		beaconDetail.descriptions[0] = beacon.description
	}else{
		beaconDetail.descriptionTitles = beacon.titleDescription
		beaconDetail.descriptions = beacon.description
	}

	return beaconDetail
}

function checkChanges(oldInfo, newInfo){
	for (var key in newInfo) {
  		var oldValue = "" + oldInfo[key]
  		var newValue  = "" + newInfo[key]
  		if (oldValue != newValue){
  			return true
  		}
	}
	return false
}

//Check if object is empty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

module.exports = function(db){
	_db = db
	return router
}