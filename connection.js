var mongodb = require('mongodb');

module.exports = {

	connect : function(){
		var server = new mongodb.Server('localhost', 27017, {
        	    auto_reconnect: true         
    	    });
   		var db = new mongodb.Db('iBeacons', server, {
        	    w: 1
    	    });
   		db.open(function (err, db) {
   			if (err){
   				console.log("Algo no funcionó")
   			}
        })

 	  	return db
	}
}