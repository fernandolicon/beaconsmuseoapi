var express = require('express')
var app = express()

var mongodb = require('mongodb');

//Custom class that performs connection to MongoDB
var connection = require('./connection.js')

app.get('/', function (req, res) {
	var db = connection.connect()
	var collection = db.collection("testData")
	console.log(collection)
	res.send('Hello World!')
	db.close()
})

//How to get parameters by URL
app.get('/tumama/:id', function (req, res) {
  res.send('La tuya!' + req.params.id)
})

var server = app.listen(3000, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port)

})